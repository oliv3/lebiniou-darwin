/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "constants.h"
#include "context.h"
#include "parameters.h"


uint32_t version = 0;
uint32_t options = BO_IMAGE;
enum LayerMode mode = LM_OVERLAY;
char dname[] = "Image";
char desc[] = "Display the current image";


void
run(Context_t *ctx)
{
  Buffer8_copy(ctx->imgf->cur->buff, passive_buffer(ctx));
}
