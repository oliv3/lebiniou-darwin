/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCANLINE_H
#define __SCANLINE_H

#include "parameters.h"


// scanline direction
static enum Direction direction;
static int current_direction;
// line thickness
static int thickness;
static uint16_t size;

// compatibility for the scanline_y plugin
#ifdef __SCANLINE_Y
#define LEFTWARDS  DOWNWARDS
#define RIGHTWARDS UPWARDS
#endif


json_t *
get_parameters(const uint8_t fetch_all)
{
  json_t *params = json_object();

  plugin_parameters_add_string_list(params, BPP_DIRECTION, DIRECTION_NB, direction_list, direction, DIRECTION_NB-1, "Direction");
  plugin_parameters_add_int(params, BPP_THICKNESS, thickness, 1, 10, 1, "Line thickness");

  return params;
}


static void
set_size()
{
  size = MAX(SIZE * thickness / 100.0, 1);
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_DIRECTION, DIRECTION_NB, direction_list, (int *)&direction) & PLUGIN_PARAMETER_CHANGED) {
    switch (direction) {
    case LEFTWARDS:
      current_direction = -1;
      break;

    case RIGHTWARDS:
      current_direction = +1;
      break;

    case BOUNCE:
    default:
      current_direction = b_rand_boolean() ? -1 : +1;
      break;
    }
  }

  if (plugin_parameter_parse_int_range(in_parameters, BPP_THICKNESS, &thickness) & PLUGIN_PARAMETER_CHANGED) {
    set_size();
  }
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters, const uint8_t fetch_all)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters(fetch_all);
}


#endif /* __SCANLINE_H */
