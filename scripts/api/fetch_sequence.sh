#!/usr/bin/env bash
#
# Fetch the current sequence on [<host> [:<port>]] (default: localhost:30543)
# and stores it in ~/.lebiniou/sequences
#
# Requires: curl, jq
#
CURL=curl
JQ=jq

for cmd in $CURL $JQ; do
    command -v $cmd >/dev/null 2>&1 || { echo >&2 "$cmd not found, aborting."; exit 1; }
done

HOST=localhost
PORT=30543

if (( $# > 0 )); then
    HOST=$1
    if (( $# > 1 )); then
        PORT=$2
    fi
fi

FILE=$(mktemp)
$CURL --silent --output $FILE http://$HOST:$PORT/sequence
# extract sequence id
ID=`$JQ '.id' $FILE`
OUTPUT="$HOME/.lebiniou/sequences/$ID.json"
mv $FILE $OUTPUT
echo "Wrote $OUTPUT"
