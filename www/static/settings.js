/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let gWidth, gHeight;
let gJackaudioLeft = "system:capture_1", gJackaudioRight = "system:capture_2";
let gWebcams, gMaxCams;
let pluginsSelect, themesSelect;


function start() {
    console.info("Entering settings");
    get_settings();
}


function get_settings() {
    fetch('/settings').then(response => response.json()).then(settings => {
        console.info(settings);
        set_version(settings.version);
        set_configuration_file(settings.file);
        set_input(settings.input);
        set_screen(settings.screen);
        set_plugins(settings.plugins);
        set_themes(settings.themes);
        set_engine(settings.engine, settings.currentSettings.engine);
        set_misc(settings.misc);
    });
}


function set_version(version) {
    $('#lb_version').val(version);
}


function set_configuration_file(file) {
    if (!file.includes(".json")) {
        file += ".json";
    }
    $('#lb_configuration_file').html(file);
    $('#lb_file').val(file);
}


function set_input(input) {
    const { name, allInputPlugins, volumeScale } = input;
    $('#lbVolumeScale').val(volumeScale.toFixed(1));
    if (name !== "jackaudio") {
        $('.lb-jackaudio-container').hide();
    }
    let select = '<b class="lb-setting">Input:&nbsp;</b><select name="name" onchange="input_change(this.value);">';
    for (let i in allInputPlugins) {
        if (![ "NULL", "sndfile" ].includes(allInputPlugins[i])) {
            select += `<option value="${allInputPlugins[i]}"`;
            select += (allInputPlugins[i] === name) ? ' selected' : '';
            select += `>${allInputPlugins[i]}</option>`;
        }
    }
    select += '</select>';
    $('#lb_input').html(select);
    if (name === "jackaudio") {
        const { jackaudioLeft, jackaudioRight } = input;
        gJackaudioLeft = jackaudioLeft;
        gJackaudioRight = jackaudioRight;
        $('#lb_jackaudio_left').val(jackaudioLeft);
        $('#lb_jackaudio_right').val(jackaudioRight);
    }
}


function input_change(name) {
    if (name === "jackaudio") {
        $('.lb-jackaudio-container').show();
        $('#lb_jackaudio_left').val(gJackaudioLeft);
        $('#lb_jackaudio_right').val(gJackaudioRight);
    } else {
        $('.lb-jackaudio-container').hide();
    }
}


const screen_resolutions_names = {
    '640x360': 'nHD',
    '800x600': 'SVGA',
    '960x540': 'qHD',
    '1024x768': 'XGA',
    '1280x720': 'WXGA',
    '1280x1024': 'SXGA',
    '1366x768': 'HD',
    '1440x900': 'WXGA+',
    '1600x900': 'HD+',
    '1680x1050': 'WSXGA+',
    '1920x1080': 'FHD',
    '1920x1200': 'WUXGA',
    '2048x1152': 'QWXGA',
    '2560x1440': 'QHD',
    '3840x2160': '4K UHD'
}
const screen_resolutions = {
    'nHD': [ 640, 360 ],
    'SVGA': [ 800, 600 ],
    'qHD': [ 960, 540 ],
    'XGA': [ 1024, 768 ],
    'WXGA': [ 1280, 720 ],
    'SXGA': [ 1280, 1024 ],
    'HD': [ 1366, 768 ],
    'WXGA+': [ 1440, 900 ],
    'HD+': [ 1600, 900 ],
    'WSXGA+': [ 1680, 1050 ],
    'FHD': [ 1920, 1080 ],
    'WUXGA': [ 1920, 1200 ],
    'QWXGA': [ 2048, 1152 ],
    'QHD': [ 2560, 1440 ],
    '4K UHD': [ 3840, 2160 ]
}


function set_screen(screen) {
    if (screen.fixed) {
        $('.screen-container').hide();
    } else {
        const { width, height } = screen;
        gWidth = width;
        gHeight = height;
        $('#lb_width_input').val(width);
        $('#lb_height_input').val(height);
        build_screen_select(screen);
    }
}

function set_resolution() {
    const screen = {
        "width": $('#lb_width_input').val(),
        "height": $('#lb_height_input').val()
    }
    build_screen_select(screen);
}


function build_screen_select(screen) {
    const { width, height } = screen;
    $('#lb_width_input').val(width);
    $('#lb_height_input').val(height);
    const name = screen_resolutions_names[width + 'x' + height];
    const option = name ? name : 'custom';
    let select = '<b class="lb-setting">Display:&nbsp;</b><select onchange="resolution_change(this.value);">';
    for (let i in screen_resolutions) {
        const [ w, h ] = screen_resolutions[i];
        select += `<option name="${i}"`;
        select += (i === option) ? ' selected' : '';
        select += `>${i} (${w}x${h})</option>`;
    }
    select += '<option name="custom"';
    select += (option === 'custom') ? ' selected' : '';
    select += '>Custom</option>';
    select += '</select>';
    $('#lb_resolution').html(select);
}


function resolution_change(s) {
    if (s !== 'Custom') {
        const [ w, h ] = s.split('(')[1].split(')')[0].split('x');
        build_screen_select({ width: w, height: h });
    } else {
        build_screen_select({ width: gWidth, height: gHeight });
    }
}


function set_plugins(plugins) {
    plugins.sort((a, b) => b.name < a.name);
    let select = '<b class="lb-setting">Plugins:&nbsp;</b><select id="lbPlugins" name="plugins" multiple>';
    for (let i in plugins) {
        const p = plugins[i];
        select += `<option value="${p.name}"`;
        select += p.enabled ? ' selected' : '';
        select += `>${p.display_name}</option>`;
    }
    select += '</select>';
    $('#lb_plugins').html(select);
    pluginsSelect = new vanillaSelectBox("#lbPlugins", {
        search: true
    });
}


function set_themes(themes) {
    const { baseThemes, selectedThemes, userThemes } = themes;
    let options = [];
    baseThemes.map(t => options.push({ "value": t, "name": t}));
    userThemes.map(t => options.push({ "value": '~' + t, "name": t}));
    options.sort((a, b) => b.name < a.name);
    let select = '<b class="lb-setting">Themes:&nbsp;</b><select id="lbThemes" name="themes" multiple>';
    for (let i in options) {
        const o = options[i];
        select += `<option value="${o.value}"`;
        select += selectedThemes.includes(o.value) ? ' selected' : '';
        select += `>${o.name}</option>`;
    }
    select += '</select>';
    select += '<p>Add more themes by putting images in any <span class="formatted">~/.lebiniou/images/</span> subdirectory. ';
    select += 'Example: <span class="formatted"><span id="lbHomeDir"></span>/.lebiniou/images/my_cool_theme/my_cool_image_1.png</span>';
    select += ' will add <span class="formatted">my_cool_theme</span> to the list.</p>';
    $('#lbThemesContainer').html(select);
    themesSelect = new vanillaSelectBox("#lbThemes", {
        search: true
    });
}


function set_engine(engine, currentEngine) {
    const {
        startMode,
        autoColormapsMode, colormapsMin, colormapsMax,
        autoImagesMode, imagesMin, imagesMax,
        webcams, autoWebcamsMode, webcamsMin, webcamsMax, maxCams, hFlip, vFlip,
        autoSequencesMode, sequencesMin, sequencesMax,
        randomMode, maxFps, fadeDelay,
        flatpak
    } = engine;

    $('#lbStartMode').val(startMode);
    $('#lbHFlip').prop("checked", hFlip);
    $('#lbVFlip').prop("checked", vFlip);
    $('#lbFlatpak').val(flatpak);
    if (flatpak) {
        $('#lbCommand').html("flatpak run net.biniou.LeBiniou");
    } else {
        $('#lbCommand').html("lebiniou");
    }

    $('#lbMaxFps').val(maxFps);
    $('#lbFadeDelay').val(fadeDelay);
    $('#lbRandomMode').val(randomMode);
    $('#lbAutoSequencesMode').val(autoSequencesMode);
    $('#lbAutoColormapsMode').val(autoColormapsMode);
    $('#lbAutoImagesMode').val(autoImagesMode);
    $('#lbAutoWebcamsMode').val(autoWebcamsMode);
    $('#lbSequencesMin').val(sequencesMin);
    $('#lbSequencesMax').val(sequencesMax);
    $('#lbColormapsMin').val(colormapsMin);
    $('#lbColormapsMax').val(colormapsMax);
    $('#lbImagesMin').val(imagesMin);
    $('#lbImagesMax').val(imagesMax);
    gWebcams = webcams;
    gMaxCams = maxCams;
    $('#lbWebcams').val(webcams);
    $('#lbWebcams').prop("max", maxCams);
    $('#lbWebcamsMin').val(webcamsMin);
    $('#lbWebcamsMax').val(webcamsMax);
    if (webcams < 2) {
        $('#lbAutoWebcamsRow').hide();
    } else {
        $('#lbAutoWebcamsRow').show();
    }
    if (currentEngine) {
        const {
            autoSequencesMode, autoColormapsMode, autoImagesMode, autoWebcamsMode,
            webcams
        } = currentEngine;
        $('#lbAutoSequencesMode').val(autoSequencesMode);
        $('#lbAutoColormapsMode').val(autoColormapsMode);
        $('#lbAutoImagesMode').val(autoImagesMode);
        $('#lbAutoWebcamsMode').val(autoWebcamsMode);
        $('#lbWebcams').val(webcams);
        if (webcams < 2) {
            $('#lbAutoWebcamsRow').hide();
        } else {
            $('#lbAutoWebcamsRow').show();
        }
    }
}


function post_form() {
    if (!check_range("#lbMaxFps", 1, 255)) return;
    if (!check_range("#lbFadeDelay", 1, 255)) return;
    if (!check_range("#lbWebcams", 0, gMaxCams)) return;
    if (!check_min_max("#lbSequencesMin", "#lbSequencesMax")) return;
    if (!check_min_max("#lbColormapsMin", "#lbColormapsMax")) return;
    if (!check_min_max("#lbImagesMin", "#lbImagesMax")) return;
    if ((gWebcams > 1) && !check_min_max("#lbWebcamsMin", "#lbWebcamsMax")) return;
    if (!check_volume_scale()) return;
    const form = JSON.stringify($('#lb_settings').serializeArray());
    pluginsSelect.destroy();
    themesSelect.destroy();
    fetch('/settings.html', {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: form
    }).then(data  => data.json()).then(res => {
        if (res.restart) {
            alert("You need to restart Le Biniou for the changes to take effect.");
        }
        let date = new Date();
        window.location=`/settings.html?t=${date.getTime()}`;
    });
}


function check_range(field, min, max) {
    const val = +$(field).val();

    if (val < min) {
        alert(`${val} must be >= ${min}`);
        return 0;
    } else if (val > max) {
        alert(`${val} must be <= ${max}`);
        return 0;
    } else {
        return 1;
    }
}

function check_min_max(min, max) {
    const minVal = +$(min).val();
    const maxVal = +$(max).val();

    if (minVal < 1) {
        alert(`${min} must be >= 1`);
        return 0;
    }
    if (maxVal < 1) {
        alert(`${max} must be >= 1`);
        return 0;
    }
    if (minVal > 60) {
        alert(`${min} must be <= 60`);
        return 0;
    }
    if (maxVal > 60) {
        alert(`${max} must be <= 1`);
        return 0;
    }
    if (minVal > maxVal) {
        alert(`${min} must be <= ${max}`);
        return 0;
    }

    return 1;
}


function set_misc(misc) {
    const { homeDir, statistics, desktopSymlink } = misc;
    $('#lbHomeDir').html(homeDir);
    $('#lbStatistics').prop("checked", statistics);
    if (desktopSymlink) {
        $('#lbSymlink').hide();
    }
}


function check_volume_scale() {
    if ($('#lbVolumeScale').val() <= 0) {
        alert("Volume scale must be > 0");
        return 0;
    }
    return 1;
}
