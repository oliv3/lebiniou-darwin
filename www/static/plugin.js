/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function plugin_command_result(r) {
    // console.log("plugin_command_result:", r);
    switch (r.command) {
    case "CMD_SEQ_SELECT_NEXT":
    case "CMD_SEQ_SELECT_PREVIOUS":
    case "CMD_PLG_NEXT":
    case "CMD_PLG_PREVIOUS":
    case "CMD_PLG_SCROLL_DOWN":
    case "CMD_PLG_SCROLL_UP":
        selected_plugin = r.result.selected_plugin;
        $('#lb_selected_plugin_dname').html(r.result.selected_plugin_dname);
        parameters(r.result);
        return 1;

    case "CMD_APP_TOGGLE_SELECTED_PLUGIN":
        set_sequence(r.result.sequence);
        return 1;

    case "CMD_APP_LOCK_SELECTED_PLUGIN":
        set_locked(r.result.locked_plugin);
        return 1;

    case "UI_CMD_CONNECT":
        if (r.result.locked_plugin) {
            set_locked(r.result.selected_plugin === r.result.locked_plugin);
        } else {
            set_locked(false);
        }
        return 1;
    }

    switch(r.ui_command) {
    case "UI_CMD_APP_SELECT_PLUGIN":
        parameters(r.result);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE":
        parameters(r.result);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE":
        $('#lb_parameter_value_' + r.result.selected_param).html(format_playlist(r.result.value));
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_SELECT_VALUE":
        $('#lb_param_select_' + r.result.selected_param).val(r.result.value);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE":
        $(`#lb_param_slider_${r.result.selected_param}`).slider('value', r.result.value);
        if (r.result.type === "integer") {
            $(`#lb_parameter_value_${r.result.selected_param}`).html(r.result.value);
        } else { // double
            $(`#lb_parameter_value_${r.result.selected_param}`).html(r.result.value / 1000);
        }
        return 1;
    }

    return 0;
}


function fmt(s) {
    s = s.charAt(0).toUpperCase() + s.slice(1);

    return s.replace(/_/g, ' ');
}


function parameters(p) {
    if (p.parameters) {
        const params = p.parameters;

        if (params) {
            let html = '';
            const keys = Object.keys(params);

            $('#lb_parameters').empty();
            for (let i in keys) {
                const key = keys[i];
                const param = params[key];

                html += parameter_html('lb_param', i, key, param);
            }
            $('#lb_parameters').append(html);
            $('#lb_parameters').show();

            for (let i in keys) {
                const key = keys[i];
                const param = params[key];

                if ((param.type === "integer") || (param.type === "double")) {
                    const is_double = (param.type === "double");
                    $(`#lb_param_slider_${i}`).slider({
                        min: is_double ? param.min * 1000 : param.min,
                        max: is_double ? param.max * 1000 : param.max,
                        step: is_double ? param.step * 1000 : param.step,
                        value: is_double ? param.value * 1000 : param.value,
                        animate: "fast",
                        orientation: "horizontal",
                        slide: function(event, ui) {
                            let id = $(this).attr('id');
                            id = id.replace(/^lb_param_slider_/, '');
                            ui_command('UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE', { 'selected_param': Number(id), 'value': ui.value });
                        }
                    });
                } else if (param.type === "playlist") {
                    $(`#lb_param_playlist_${i}`).change(function(e) {
                        let id = $(this).attr('id');
                        id = id.replace(/^lb_param_playlist_/, '');
                        let playlist = [];
                        for (let i in e.target.files) {
                            playlist.push(e.target.files[i].name);
                        }
                        ui_command('UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE', { 'selected_param': + Number(id), 'value': playlist });
                    });
                    $('#lb_param_playlist_value_' + i).html(param.value);
                }
            }
        } else {
            $('#lb_parameters').hide();
        }
    } else {
        $('#lb_parameters').hide();
    }
}


function parameter_html(prefix, index, name, param) {
    let html = '';

    if (param.type === "boolean") {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><input type="checkbox" id="${prefix}_checkbox_${index}" class="lb-parameter-checkbox" onchange="ui_command(\'UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE\', { \'selected_param\': ${+index}, \'value\': this.checked });" ${param.value ? ' checked' : ''}`;
        html += param.description ? ` title="${param.description}"` : '';
        html += ' /></div>';
    } else if ((param.type === "integer") || (param.type === "double")) {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><div id="lb_parameter_value_${index}" class="lb-parameter-value">${param.value}</div><div id="${prefix}_slider_${index}"`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '></div></div>';
    } else if (param.type === "string_list") {
        const value_list = param.value_list;

        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><select class="lb-string-list" id="${prefix}_select_${index}" class="lb-parameter-string-list" onchange="ui_command(\'UI_CMD_SEQ_SET_PARAM_SELECT_VALUE\', { \'selected_param\': ${+index}, \'value\': this.value });"`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '>';
        for (let j in value_list) {
            const selected = (param.value === value_list[j]) ? ' selected': '';
            html += `<option value="${value_list[j]}" ${selected}>${value_list[j]}</option>`;
        }
        html += '</select></div>';
    } else if (param.type === "playlist") {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><div id="lb_parameter_value_${index}" class="lb-parameter-value">${format_playlist(param.value)}</div><div><input type="file" id="${prefix}_playlist_${index}" multiple`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '></div></div>';
    }

    return html;
}

function change_param(param, value) {
    ui_command('UI_CMD_SEQ_SET_INTEGER_PARAM_VALUE', { "param": param, "value": +value });
}


function set_locked(p) {
    if (p) {
        $('#lb_locked').html('&#x1f512;');
    } else {
        $('#lb_locked').html('&#x1f513;');
    }
}


function format_playlist(p) {
    let html = "<ol>";

    for (let i in p) {
        html += `<li>${p[i]}</li>`;
    }

    html += "</ol>";
    return html;
}
