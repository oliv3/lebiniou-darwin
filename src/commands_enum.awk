BEGIN {
    print "enum Command {";
    N = 0;
    SECTION = "";
}


{
    if ($1 == "*") {
        SECTION = $2;
        if (N == 0)
            print "  START_"$2" = 0,";
        else
            print "  START_"$2",";
        N = N + 1;
    } else if ($1 == "**") {
        print "  END_"SECTION",";
    } else if (($1 != "#") && ($0 != "")) {
        print "  "$4",";
    }
}


END {
    print "};";
    print "";
}
